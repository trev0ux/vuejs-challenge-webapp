# VueJS Challenge

## 1. Crie uma branch com o seu nome, onde irá subir seus commits.

## 2. Desenvolva uma aplicação web utilizando VueJS, que resolva o problema descrito na [issue 1](https://gitlab.com/jaisonperes/vuejs-challenge-webapp/-/issues/1)

## 3. Envie o link da sua branch para o email contato@qcedtech.com com o assunto "VueJs Challenge".